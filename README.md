# README #

This is an example project to implement an URL shortener.

The concept is to use S3 object redirect for every short URL.

A website will allow to enter a URL (e.g http://google.com/) and creates a short URL.
The website invokes a REST Api which creates a short URL, puts this as a new file in the S3 bucket and configures the redirect URL as MetaData.

### Installation ###

Use serverless framework to deploy the solution.

https://serverless.com/framework/docs/getting-started/

In order to deploy to AWS the access key and the secret need to be available.

    export AWS_ACCESS_KEY_ID=<your-key-here>
    export AWS_SECRET_ACCESS_KEY=<your-secret-key-here>

Once configured run the serverless deploy command with a unique name for a new S3 bucket

    serverless deploy --s3bucket <unique-s3-name>

The script will create a new Cloudformation stack in the Sydney region. The result can be viewed here:
https://ap-southeast-2.console.aws.amazon.com/cloudformation

In the output section is the ConnectURL which opens the admin interface for the URL shortener.
